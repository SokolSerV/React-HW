import React from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

export default class App extends React.Component {
    state = { isModalFirst: false, isModalSecond: false };

    handlerModal = (modalVision) => {
        this.setState({
            [modalVision]: !this.state[modalVision],
        });
        // console.log(modalVision, this.state[modalVision]);
    };

    render() {
        return (
            <React.Fragment>
                <div className="buttonWrapper">
                    <Button
                        text={"Open first modal"}
                        backgroundColor={"red"}
                        onClick={() => this.handlerModal("isModalFirst")}
                        isDisabled={this.state.isModalSecond ? true : false}
                    ></Button>
                    <Button
                        text={"Open second modal"}
                        backgroundColor={"blue"}
                        onClick={() => this.handlerModal("isModalSecond")}
                        isDisabled={this.state.isModalFirst ? true : false}
                    ></Button>
                </div>
                {this.state.isModalFirst && (
                    <Modal
                        modalID={"isModalFirst"}
                        header={"Do you want to delete this file?"}
                        closeButton
                        closeModal={this.handlerModal}
                        text={
                            "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
                        }
                        actions={
                            <>
                                <button className="modal-button" type="button">
                                    Ok
                                </button>
                                <button
                                    className="modal-button"
                                    type="button"
                                    onClick={() =>
                                        this.handlerModal("isModalFirst")
                                    }
                                >
                                    Cancel
                                </button>
                            </>
                        }
                    />
                )}
                {this.state.isModalSecond && (
                    <Modal
                        modalID={"isModalSecond"}
                        header={"На цьому місці має бути Header"}
                        closeButton
                        closeModal={this.handlerModal}
                        text={
                            "Увага! Для другого модального вікна потрібно використовувати інший текст!"
                        }
                        actions={
                            <>
                                <button className="modal-button" type="button">
                                    Start
                                </button>
                                <button
                                    className="modal-button"
                                    type="button"
                                    onClick={() =>
                                        this.handlerModal("isModalSecond")
                                    }
                                >
                                    Finish
                                </button>
                            </>
                        }
                    />
                )}
            </React.Fragment>
        );
    }
}
