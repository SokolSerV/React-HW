import React from "react";
import "./Button.scss";

export default class Button extends React.Component {
    render() {
        const { backgroundColor, text, onClick, isDisabled } = this.props;

        return (
            <button
                className="button"
                style={{ backgroundColor }}
                onClick={onClick}
                disabled={isDisabled}
            >
                {text}
            </button>
        );
    }
}
