import React from "react";

import Header from "./components/Header";
import GoodsList from "./components/Goods/GoodsList";
import Modal from "./components/Modal";
import Button from "./components/Button";

import sendRequest from "./helpers/sendRequest";

import "./App.scss";

export default class App extends React.Component {
    state = {
        gooods: [],
        isModal: false,
        favorites: JSON.parse(localStorage.getItem("favorites")) || [],
        basket: JSON.parse(localStorage.getItem("basket")) || [],
        currentCard: {},
    };

    handlerModal = () => {
        this.setState({
            isModal: !this.state.isModal,
        });
    };

    componentDidMount() {
        sendRequest("http://localhost:3000/goods.json").then((data) => {
            this.setState({
                gooods: data,
            });
        });
    }

    componentDidUpdate() {
        if (this.state.favorites.length) {
            localStorage.setItem(
                "favorites",
                JSON.stringify(this.state.favorites)
            );
        } else {
            localStorage.removeItem("favorites");
        }

        if (this.state.basket.length) {
            localStorage.setItem("basket", JSON.stringify(this.state.basket));
        } else {
            localStorage.removeItem("basket");
        }
    }

    addFavorites = (card) => {
        const isFavorites = this.state.favorites.some(
            (elem) => elem.id === card.id
        );
        if (!isFavorites) {
            this.setState({
                favorites: [...this.state.favorites, card],
            });
        }
    };

    removeFavorites = (card) => {
        const updateFavorites = this.state.favorites.filter(
            (elem) => elem.id !== card.id
        );
        this.setState({
            favorites: updateFavorites,
        });
    };

    offerAddCard = (card) => {
        this.setState({
            currentCard: card,
        });
    };

    addBasket = (card) => {
        const isBasket = this.state.basket.some((elem) => elem.id === card.id);
        if (!isBasket) {
            this.setState({
                basket: [...this.state.basket, card],
                currentCard: {},
                isModal: !this.state.isModal,
            });
        }
    };

    render() {
        const { favorites, basket } = this.state;
        return (
            <React.Fragment>
                <Header
                    favoritesCounter={favorites.length}
                    basketCounter={basket.length}
                />
                <main className="main">
                    <GoodsList
                        goods={this.state.gooods}
                        favoritesList={this.state.favorites}
                        handlerModal={this.handlerModal}
                        addFavorites={this.addFavorites}
                        removeFavorites={this.removeFavorites}
                        offerAddCard={this.offerAddCard}
                    />
                    {this.state.isModal && (
                        <Modal
                            header={"Додавання товару в кошик"}
                            closeButton
                            closeModal={this.handlerModal}
                            text={"Додати?"}
                            actions={
                                <>
                                    <Button
                                        backgroundColor={"green"}
                                        text={"Так"}
                                        onClick={() => {
                                            this.addBasket(
                                                this.state.currentCard
                                            );
                                        }}
                                    />
                                    <Button
                                        backgroundColor={"red"}
                                        text={"Ні"}
                                        onClick={this.handlerModal}
                                    />
                                </>
                            }
                        />
                    )}
                </main>
            </React.Fragment>
        );
    }
}
