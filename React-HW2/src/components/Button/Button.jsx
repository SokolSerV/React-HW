import React from "react";
import PropTypes from "prop-types";

import "./Button.scss";

export default class Button extends React.Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;

        return (
            <button
                className="button"
                style={{ backgroundColor }}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    backgroundColor: "grey",
    text: "Кнопка",
    onClick: () => {},
};
