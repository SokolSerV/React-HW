import React from "react";
import PropTypes from "prop-types";

import GoodsCard from "../GoodsCard/GoodsCard";

import "./GoodsList.scss";

export default class GoodsList extends React.Component {
    render() {
        const {
            goods,
            handlerModal,
            addFavorites,
            removeFavorites,
            favoritesList,
            offerAddCard,
        } = this.props;

        const arrayGoods = goods.map((card) => {
            return (
                // <React.Fragment key={card.id}>
                <GoodsCard
                    key={card.id}
                    card={card}
                    handlerModal={handlerModal}
                    addFavorites={addFavorites}
                    removeFavorites={removeFavorites}
                    favoritesList={favoritesList}
                    offerAddCard={offerAddCard}
                />
                // </React.Fragment>
            );
        });
        return <div className="goods_wrapper">{arrayGoods}</div>;
    }
}

GoodsList.propTypes = {
    goods: PropTypes.array,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    favoritesList: PropTypes.array,
    offerAddCard: PropTypes.func,
};

GoodsList.defaultProps = {
    goods: [],
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    favoritesList: [],
    offerAddCard: () => {},
};
