import { useState, useEffect } from "react";
import AppRoutes from "./routes";

import sendRequest from "./helpers/sendRequest";

import "./App.scss";

export default function App() {
    const [goods, setGooods] = useState([]);
    const [favoritesList, setFavoritesList] = useState(
        JSON.parse(localStorage.getItem("favorites")) || []
    );
    const [basketList, setBasketList] = useState(
        JSON.parse(localStorage.getItem("basket")) || []
    );
    const [currentCard, setCurrentCard] = useState({});

    useEffect(() => {
        sendRequest("http://localhost:3000/goods.json").then((data) => {
            setGooods(data);
        });
    }, []);

    useEffect(() => {
        if (favoritesList.length) {
            localStorage.setItem("favorites", JSON.stringify(favoritesList));
        } else {
            localStorage.removeItem("favorites");
        }

        if (basketList.length) {
            localStorage.setItem("basket", JSON.stringify(basketList));
        } else {
            localStorage.removeItem("basket");
        }
    }, [favoritesList, basketList]);

    const addFavorites = (card) => {
        const isFavorites = favoritesList.some((elem) => elem.id === card.id);
        if (!isFavorites) {
            setFavoritesList([...favoritesList, card]);
        }
    };

    const removeFavorites = (card) => {
        const updateFavorites = favoritesList.filter(
            (elem) => elem.id !== card.id
        );
        setFavoritesList(updateFavorites);
        localStorage.setItem("favorites", JSON.stringify(updateFavorites));
    };

    const offerCard = (card) => {
        setCurrentCard(card);
    };

    const addBasket = (card) => {
        const isBasket = basketList.some((elem) => elem.id === card.id);
        if (!isBasket) {
            setBasketList([...basketList, card]);
            setCurrentCard({});
        }
    };

    const removeBasket = (card) => {
        const updateBasket = basketList.filter((elem) => elem.id !== card.id);
        setBasketList(updateBasket);
        localStorage.setItem("basket", JSON.stringify(updateBasket));
    };

    return (
        <AppRoutes
            goods={goods}
            favoritesList={favoritesList}
            basketList={basketList}
            addFavorites={addFavorites}
            removeFavorites={removeFavorites}
            offerCard={offerCard}
            currentCard={currentCard}
            addBasket={addBasket}
            removeBasket={removeBasket}
        />
    );
}
