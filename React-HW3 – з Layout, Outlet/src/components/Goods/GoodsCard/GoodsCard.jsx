import { useState, useEffect } from "react";
import PropTypes from "prop-types";

import Button from "../../Button";
import { ReactComponent as Select } from "./icons/select.svg";
import { ReactComponent as Delete } from "./icons/skullCrossbones.svg";

import "./GoodsCard.scss";

export default function GoodsCard({
    card,
    favoritesList,
    basketList,
    handlerModal,
    addFavorites,
    removeFavorites,
    offerCard,
    btnAddBasket,
    btnDeleteBasket,
}) {
    const [isSelected, setIsSelected] = useState(false);
    const [isBasket, setIsBasket] = useState(false);

    useEffect(() => {
        setIsSelected(favoritesList.some((elem) => elem.id === card.id));
        setIsBasket(basketList.some((elem) => elem.id === card.id));
    }, [favoritesList, basketList, card.id]);

    const { name, price, image, article, color, id } = card;

    return (
        <div className="card" id={id}>
            <h4 className="card_title">{name}</h4>
            <img
                className="card_img"
                src={image}
                alt={name}
                width="250"
                height="250"
            />
            <p className="card_article">Артикул: {article}</p>
            <p className="card_color">Колір - {color}</p>
            <p className="card_price">Ціна - {price} грн.</p>
            <div className="card_actions">
                <span
                    className={
                        isSelected ? "card_selected--active" : "card_selected"
                    }
                    onClick={() => {
                        setIsSelected(!isSelected);
                        if (isSelected) {
                            removeFavorites(card);
                        } else {
                            addFavorites(card);
                        }
                    }}
                >
                    <Select />
                </span>
                {btnAddBasket && (
                    <Button
                        backgroundColor={isBasket ? "grey" : "blue"}
                        text={isBasket ? "Товар у кошику" : "Додати в кошик"}
                        onClick={() => {
                            handlerModal();
                            offerCard(card);
                        }}
                        isDisabled={isBasket}
                    />
                )}
                {btnDeleteBasket && (
                    <span
                        className={"card_selected"}
                        onClick={() => {
                            handlerModal();
                            offerCard(card);
                        }}
                    >
                        <Delete />
                    </span>
                )}
            </div>
        </div>
    );
}

GoodsCard.propTypes = {
    key: PropTypes.number,
    card: PropTypes.object,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    favoritesList: PropTypes.array,
    offerCard: PropTypes.func,
};

GoodsCard.defaultProps = {
    card: {},
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    favoritesList: [],
    offerCard: () => {},
};
