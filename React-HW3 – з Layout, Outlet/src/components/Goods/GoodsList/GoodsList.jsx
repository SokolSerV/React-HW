import React from "react";
import PropTypes from "prop-types";

import GoodsCard from "../GoodsCard";

import "./GoodsList.scss";

export default function GoodsList({
    goods,
    favoritesList,
    basketList,
    handlerModal,
    addFavorites,
    removeFavorites,
    offerCard,
    btnAddBasket,
    btnDeleteBasket,
}) {
    const arrayGoods = goods.map((card) => {
        return (
            // <React.Fragment key={card.id}>
            <GoodsCard
                key={card.id}
                card={card}
                handlerModal={handlerModal}
                addFavorites={addFavorites}
                removeFavorites={removeFavorites}
                favoritesList={favoritesList}
                basketList={basketList}
                offerCard={offerCard}
                btnAddBasket={btnAddBasket}
                btnDeleteBasket={btnDeleteBasket}
            />
            // </React.Fragment>
        );
    });

    return <div className="goods_wrapper">{arrayGoods}</div>;
}

GoodsList.propTypes = {
    goods: PropTypes.array,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    favoritesList: PropTypes.array,
    offerCard: PropTypes.func,
};

GoodsList.defaultProps = {
    goods: [],
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    favoritesList: [],
    offerCard: () => {},
};
