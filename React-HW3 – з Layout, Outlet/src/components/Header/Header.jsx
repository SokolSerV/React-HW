import PropTypes from "prop-types";

import { ReactComponent as Logo } from "./icons/logo.svg";
import { ReactComponent as Favorites } from "./icons/favorites.svg";
import { ReactComponent as Basket } from "./icons/basket.svg";
import Navigation from "../Navigation";

import "./Header.scss";

export default function Header({ favoritesCounter, basketCounter }) {
    return (
        <header className="header">
            <div className="header_wrapper">
                <span className="header_logo">
                    <Logo />
                </span>
                <p className="header_name">Сторінка інтернет-магазину</p>
                <Navigation />
                <div className="header_icons">
                    <span className="favorites">
                        <Favorites />
                        <span className="counter">{favoritesCounter}</span>
                    </span>
                    <span className="basket">
                        <Basket />
                        <span className="counter">{basketCounter}</span>
                    </span>
                </div>
            </div>
        </header>
    );
}

Header.propTypes = {
    favoritesCounter: PropTypes.number,
    basketCounter: PropTypes.number,
};
