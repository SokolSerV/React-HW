import { Outlet } from "react-router-dom";
import Header from "../../components/Header";

export default function Layout(props) {
    const { favoritesList, basketList } = props;

    return (
        <>
            <Header
                favoritesCounter={favoritesList.length}
                basketCounter={basketList.length}
            />
            <main className="main">
                <Outlet />
            </main>
        </>
    );
}
