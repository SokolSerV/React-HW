import { useState } from "react";

import GoodsList from "../../components/Goods/GoodsList";
import Modal from "../../components/Modal";
import Button from "../../components/Button";

export default function PageBasket(props) {
    const {
        favoritesList,
        basketList,
        addFavorites,
        removeFavorites,
        offerCard,
        removeBasket,
        currentCard,
    } = props;

    const currentBasket = JSON.parse(localStorage.getItem("basket")) || [];

    const [isModal, setIsModal] = useState(false);
    const handlerModal = () => {
        setIsModal(!isModal);
    };

    return (
        <>
            <GoodsList
                goods={currentBasket}
                favoritesList={favoritesList}
                basketList={basketList}
                handlerModal={handlerModal}
                addFavorites={addFavorites}
                removeFavorites={removeFavorites}
                offerCard={offerCard}
                btnDeleteBasket
            />
            {isModal && (
                <Modal
                    header={"Видалення товару з кошика"}
                    closeButton
                    closeModal={handlerModal}
                    text={"Видалити?"}
                    actions={
                        <>
                            <Button
                                backgroundColor={"green"}
                                text={"Так"}
                                onClick={() => {
                                    removeBasket(currentCard);
                                    handlerModal();
                                }}
                            />
                            <Button
                                backgroundColor={"red"}
                                text={"Ні"}
                                onClick={handlerModal}
                            />
                        </>
                    }
                />
            )}
        </>
    );
}
