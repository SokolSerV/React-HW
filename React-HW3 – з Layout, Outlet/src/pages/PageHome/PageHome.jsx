import { useState } from "react";

import GoodsList from "../../components/Goods/GoodsList";
import Modal from "../../components/Modal";
import Button from "../../components/Button";

export default function PageHome(props) {
    const {
        goods,
        favoritesList,
        basketList,
        addFavorites,
        removeFavorites,
        offerCard,
        currentCard,
        addBasket,
    } = props;

    const [isModal, setIsModal] = useState(false);
    const handlerModal = () => {
        setIsModal(!isModal);
    };

    return (
        <>
            <GoodsList
                goods={goods}
                favoritesList={favoritesList}
                basketList={basketList}
                handlerModal={handlerModal}
                addFavorites={addFavorites}
                removeFavorites={removeFavorites}
                offerCard={offerCard}
                btnAddBasket
            />
            {isModal && (
                <Modal
                    header={"Додавання товару в кошик"}
                    closeButton
                    closeModal={handlerModal}
                    text={"Додати?"}
                    actions={
                        <>
                            <Button
                                backgroundColor={"green"}
                                text={"Так"}
                                onClick={() => {
                                    addBasket(currentCard);
                                    handlerModal();
                                }}
                            />
                            <Button
                                backgroundColor={"red"}
                                text={"Ні"}
                                onClick={handlerModal}
                            />
                        </>
                    }
                />
            )}
        </>
    );
}
