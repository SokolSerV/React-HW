import { Routes, Route } from "react-router-dom";

import Layout from "../pages/Layout";
import PageHome from "../pages/PageHome";
import PageFavorites from "../pages/PageFavorites";
import PageBasket from "../pages/PageBasket";
import NotPage from "../pages/NotPage";

export default function AppRoutes(props) {
    const {
        goods,
        favoritesList,
        basketList,
        addFavorites,
        removeFavorites,
        offerCard,
        currentCard,
        addBasket,
        removeBasket,
    } = props;

    return (
        <Routes>
            <Route
                path="/"
                element={
                    <Layout
                        favoritesList={favoritesList}
                        basketList={basketList}
                    />
                }
            >
                <Route
                    index
                    element={
                        <PageHome
                            goods={goods}
                            favoritesList={favoritesList}
                            basketList={basketList}
                            addFavorites={addFavorites}
                            removeFavorites={removeFavorites}
                            offerCard={offerCard}
                            currentCard={currentCard}
                            addBasket={addBasket}
                        />
                    }
                />
                <Route
                    path="favorites"
                    element={
                        <PageFavorites
                            favoritesList={favoritesList}
                            basketList={basketList}
                            removeFavorites={removeFavorites}
                            offerCard={offerCard}
                            currentCard={currentCard}
                            addBasket={addBasket}
                        />
                    }
                />
                <Route
                    path="basket"
                    element={
                        <PageBasket
                            favoritesList={favoritesList}
                            basketList={basketList}
                            addFavorites={addFavorites}
                            removeFavorites={removeFavorites}
                            offerCard={offerCard}
                            currentCard={currentCard}
                            removeBasket={removeBasket}
                        />
                    }
                />
                <Route path="*" element={<NotPage />} />
            </Route>
        </Routes>
    );
}
