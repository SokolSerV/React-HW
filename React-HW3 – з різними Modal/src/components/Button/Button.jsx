import PropTypes from "prop-types";
import cn from "classnames";

import "./Button.scss";

export default function Button({ backgroundColor, text, onClick, isDisabled }) {
    return (
        <button
            className={cn("button", { cursorDefault: isDisabled })}
            style={{ backgroundColor }}
            onClick={onClick}
            disabled={isDisabled}
        >
            {text}
        </button>
    );
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    isDisabled: PropTypes.bool,
};

Button.defaultProps = {
    type: "button",
    backgroundColor: "grey",
    text: "Кнопка",
    onClick: () => {},
    isDisabled: false,
};
