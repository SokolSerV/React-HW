import React from "react";
import ReactDOM from "react-dom/client";
import { ErrorBoundary } from "react-error-boundary";
import { BrowserRouter } from "react-router-dom";

import App from "./App";
import SomethingWentWrong from "./components/SomethingWentWrong/SomethingWentWrong";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <ErrorBoundary fallback={<SomethingWentWrong />}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </ErrorBoundary>
);
