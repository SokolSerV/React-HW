import PropTypes from "prop-types";

import { useState } from "react";

import GoodsList from "../../components/Goods/GoodsList";
import Modal from "../../components/Modal";
import Button from "../../components/Button";

export default function PageFavorites(props) {
    const {
        favoritesList,
        basketList,
        removeFavorites,
        offerCard,
        currentCard,
        addBasket,
    } = props;

    const currentFavorites =
        JSON.parse(localStorage.getItem("favorites")) || [];

    const [isModal, setIsModal] = useState(false);
    const handlerModal = () => {
        setIsModal(!isModal);
    };

    return (
        <>
            <GoodsList
                goods={currentFavorites}
                favoritesList={favoritesList}
                basketList={basketList}
                handlerModal={handlerModal}
                removeFavorites={removeFavorites}
                offerCard={offerCard}
                btnAddBasket
            />
            {isModal && (
                <Modal
                    header={"Додавання товару в кошик"}
                    closeButton
                    closeModal={handlerModal}
                    text={"Додати?"}
                    actions={
                        <>
                            <Button
                                backgroundColor={"green"}
                                text={"Так"}
                                onClick={() => {
                                    addBasket(currentCard);
                                    handlerModal();
                                }}
                            />
                            <Button
                                backgroundColor={"red"}
                                text={"Ні"}
                                onClick={handlerModal}
                            />
                        </>
                    }
                />
            )}
        </>
    );
}

PageFavorites.propTypes = {
    favoritesList: PropTypes.array,
    basketList: PropTypes.array,
    removeFavorites: PropTypes.func,
    offerCard: PropTypes.func,
    currentCard: PropTypes.object,
    addBasket: PropTypes.func,
};

PageFavorites.defaultPpops = {
    favoritesList: [],
    basketList: [],
    removeFavorites: () => {},
    offerCard: () => {},
    currentCard: {},
    addBasket: () => {},
};
