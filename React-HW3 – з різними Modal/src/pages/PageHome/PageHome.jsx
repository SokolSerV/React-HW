import { useState } from "react";

import PropTypes from "prop-types";

import GoodsList from "../../components/Goods/GoodsList";
import Modal from "../../components/Modal";
import Button from "../../components/Button";

export default function PageHome(props) {
    const {
        goods,
        favoritesList,
        basketList,
        addFavorites,
        removeFavorites,
        offerCard,
        currentCard,
        addBasket,
    } = props;

    const [isModal, setIsModal] = useState(false);
    const handlerModal = () => {
        setIsModal(!isModal);
    };

    return (
        <>
            <GoodsList
                goods={goods}
                favoritesList={favoritesList}
                basketList={basketList}
                handlerModal={handlerModal}
                addFavorites={addFavorites}
                removeFavorites={removeFavorites}
                offerCard={offerCard}
                btnAddBasket
            />
            {isModal && (
                <Modal
                    header={"Додавання товару в кошик"}
                    closeButton
                    closeModal={handlerModal}
                    text={"Додати?"}
                    actions={
                        <>
                            <Button
                                backgroundColor={"green"}
                                text={"Так"}
                                onClick={() => {
                                    addBasket(currentCard);
                                    handlerModal();
                                }}
                            />
                            <Button
                                backgroundColor={"red"}
                                text={"Ні"}
                                onClick={handlerModal}
                            />
                        </>
                    }
                />
            )}
        </>
    );
}

PageHome.propTypes = {
    goods: PropTypes.array,
    favoritesList: PropTypes.array,
    basketList: PropTypes.array,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    offerCard: PropTypes.func,
    currentCard: PropTypes.object,
    addBasket: PropTypes.func,
};

PageHome.defaultProps = {
    goods: [],
    favoritesList: [],
    basketList: [],
    addFavorites: () => {},
    removeFavorites: () => {},
    offerCard: () => {},
    currentCard: {},
    addBasket: () => {},
};
