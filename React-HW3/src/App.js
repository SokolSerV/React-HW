import { useState, useEffect } from "react";

import Header from "./components/Header";
import AppRoutes from "./routes/AppRoutes";
import Modal from "./components/Modal";
import Button from "./components/Button";

// import sendRequest from "./API/sendRequest";

import "./App.scss";

export default function App() {
    const [goods, setGoods] = useState([]);
    const [favoritesList, setFavoritesList] = useState(
        JSON.parse(localStorage.getItem("favorites")) || []
    );
    const [basketList, setBasketList] = useState(
        JSON.parse(localStorage.getItem("basket")) || []
    );
    const [currentCard, setCurrentCard] = useState({});

    const [isModalAdd, setIsModalAdd] = useState(false);
    const [isModalDel, setIsModalDel] = useState(false);

    const handlerModal = (modalType) => {
        if (modalType === "isModalAdd") {
            setIsModalAdd(!isModalAdd);
        }
        if (modalType === "isModalDel") {
            setIsModalDel(!isModalDel);
        }
    };

    useEffect(() => {
        try {
            // sendRequest("http://localhost:3000/goods.json").then((data) => {
            //     setGoods(data);
            // });
            (async () => {
                const data = await fetch(
                    "http://localhost:3000/goods.json"
                ).then((resp) => resp.json());
                setGoods(data);
                console.log(goods);
            })();
        } catch (error) {
            console.log(error);
        }
    }, []);

    useEffect(() => {
        if (favoritesList.length) {
            localStorage.setItem("favorites", JSON.stringify(favoritesList));
        } else {
            localStorage.removeItem("favorites");
        }

        if (basketList.length) {
            localStorage.setItem("basket", JSON.stringify(basketList));
        } else {
            localStorage.removeItem("basket");
        }
    }, [favoritesList, basketList]);

    const addFavorites = (card) => {
        const isFavorites = favoritesList.some((elem) => elem.id === card.id);
        if (!isFavorites) {
            setFavoritesList([...favoritesList, card]);
        }
    };
    const removeFavorites = (card) => {
        const updateFavorites = favoritesList.filter(
            (elem) => elem.id !== card.id
        );
        setFavoritesList(updateFavorites);
        localStorage.setItem("favorites", JSON.stringify(updateFavorites));
    };

    const offerCard = (card) => {
        setCurrentCard(card);
    };

    const addBasket = (card) => {
        const isBasket = basketList.some((elem) => elem.id === card.id);
        if (!isBasket) {
            setBasketList([...basketList, card]);
            setCurrentCard({});
        }
    };

    const removeBasket = (card) => {
        const updateBasket = basketList.filter((elem) => elem.id !== card.id);
        setBasketList(updateBasket);
        localStorage.setItem("basket", JSON.stringify(updateBasket));
    };

    return (
        <>
            <Header
                favoritesCounter={favoritesList.length}
                basketCounter={basketList.length}
            />
            <AppRoutes
                goods={goods}
                favoritesList={favoritesList}
                basketList={basketList}
                handlerModal={handlerModal}
                addFavorites={addFavorites}
                removeFavorites={removeFavorites}
                offerCard={offerCard}
            />
            {(isModalAdd || isModalDel) && (
                <Modal
                    header={
                        isModalAdd
                            ? "Додавання товару в кошик"
                            : "Видалення товару з кошика"
                    }
                    closeButton
                    closeModal={() =>
                        isModalAdd
                            ? handlerModal("isModalAdd")
                            : handlerModal("isModalDel")
                    }
                    text={isModalAdd ? "Додати?" : "Видалити?"}
                    actions={
                        <>
                            <Button
                                backgroundColor={"green"}
                                text={"Так"}
                                onClick={() => {
                                    if (isModalAdd) {
                                        addBasket(currentCard);
                                        handlerModal("isModalAdd");
                                    } else {
                                        removeBasket(currentCard);
                                        handlerModal("isModalDel");
                                    }
                                }}
                            />
                            <Button
                                backgroundColor={"red"}
                                text={"Ні"}
                                onClick={() =>
                                    isModalAdd
                                        ? handlerModal("isModalAdd")
                                        : handlerModal("isModalDel")
                                }
                            />
                        </>
                    }
                />
            )}
        </>
    );
}
