import React from "react";
import PropTypes from "prop-types";

import GoodsCard from "../GoodsCard";

import "./GoodsList.scss";

export default function GoodsList({
    goods,
    favoritesList,
    basketList,
    handlerModal,
    addFavorites,
    removeFavorites,
    offerCard,
    btnAddBasket,
    btnDeleteBasket,
}) {
    const arrayGoods = goods.map((card) => {
        return (
            <GoodsCard
                key={card.id}
                card={card}
                favoritesList={favoritesList}
                basketList={basketList}
                handlerModal={handlerModal}
                addFavorites={addFavorites}
                removeFavorites={removeFavorites}
                offerCard={offerCard}
                btnAddBasket={btnAddBasket}
                btnDeleteBasket={btnDeleteBasket}
            />
        );
    });

    return <div className="goods_wrapper">{arrayGoods}</div>;
}

GoodsList.propTypes = {
    goods: PropTypes.array,
    favoritesList: PropTypes.array,
    basketList: PropTypes.array,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    offerCard: PropTypes.func,
    btnAddBasket: PropTypes.bool,
    btnDeleteBasket: PropTypes.bool,
};

GoodsList.defaultProps = {
    goods: [],
    favoritesList: [],
    basketList: [],
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    offerCard: () => {},
    btnAddBasket: false,
    btnDeleteBasket: false,
};
