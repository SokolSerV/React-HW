import PropTypes from "prop-types";

import GoodsList from "../../components/Goods/GoodsList";

export default function PageBasket(props) {
    const {
        favoritesList,
        basketList,
        handlerModal,
        addFavorites,
        removeFavorites,
        offerCard,
    } = props;

    const currentBasket = JSON.parse(localStorage.getItem("basket")) || [];

    return (
        <GoodsList
            goods={currentBasket}
            favoritesList={favoritesList}
            basketList={basketList}
            handlerModal={handlerModal}
            addFavorites={addFavorites}
            removeFavorites={removeFavorites}
            offerCard={offerCard}
            btnDeleteBasket
        />
    );
}

PageBasket.propTypes = {
    favoritesList: PropTypes.array,
    basketList: PropTypes.array,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    offerCard: PropTypes.func,
};

PageBasket.defaultProps = {
    favoritesList: [],
    basketList: [],
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    offerCard: () => {},
};
