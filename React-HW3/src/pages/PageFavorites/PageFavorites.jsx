import PropTypes from "prop-types";

import GoodsList from "../../components/Goods/GoodsList";

export default function PageFavorites(props) {
    const {
        favoritesList,
        basketList,
        handlerModal,
        removeFavorites,
        offerCard,
    } = props;

    const currentFavorites =
        JSON.parse(localStorage.getItem("favorites")) || [];

    return (
        <GoodsList
            goods={currentFavorites}
            favoritesList={favoritesList}
            basketList={basketList}
            handlerModal={handlerModal}
            removeFavorites={removeFavorites}
            offerCard={offerCard}
            btnAddBasket
        />
    );
}

PageFavorites.propTypes = {
    favoritesList: PropTypes.array,
    basketList: PropTypes.array,
    handlerModal: PropTypes.func,
    removeFavorites: PropTypes.func,
    offerCard: PropTypes.func,
};

PageFavorites.defaultPpops = {
    favoritesList: [],
    basketList: [],
    handlerModal: () => {},
    removeFavorites: () => {},
    offerCard: () => {},
};
