import PropTypes from "prop-types";

import GoodsList from "../../components/Goods/GoodsList";

export default function PageHome(props) {
    const {
        goods,
        favoritesList,
        basketList,
        handlerModal,
        addFavorites,
        removeFavorites,
        offerCard,
    } = props;

    return (
        <GoodsList
            goods={goods}
            favoritesList={favoritesList}
            basketList={basketList}
            handlerModal={handlerModal}
            addFavorites={addFavorites}
            removeFavorites={removeFavorites}
            offerCard={offerCard}
            btnAddBasket
        />
    );
}

PageHome.propTypes = {
    goods: PropTypes.array,
    favoritesList: PropTypes.array,
    basketList: PropTypes.array,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    offerCard: PropTypes.func,
};

PageHome.defaultProps = {
    goods: [],
    favoritesList: [],
    basketList: [],
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    offerCard: () => {},
};
