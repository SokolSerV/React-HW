import { Routes, Route } from "react-router-dom";
import PropTypes from "prop-types";

import PageHome from "../pages/PageHome";
import PageFavorites from "../pages/PageFavorites";
import PageBasket from "../pages/PageBasket";
import NotPage from "../pages/NotPage";

export default function AppRoutes(props) {
    const {
        goods,
        favoritesList,
        basketList,
        handlerModal,
        addFavorites,
        removeFavorites,
        offerCard,
    } = props;

    return (
        <Routes>
            <Route
                path="/"
                element={
                    <PageHome
                        goods={goods}
                        favoritesList={favoritesList}
                        basketList={basketList}
                        handlerModal={handlerModal}
                        addFavorites={addFavorites}
                        removeFavorites={removeFavorites}
                        offerCard={offerCard}
                    />
                }
            />
            <Route
                path="favorites"
                element={
                    <PageFavorites
                        favoritesList={favoritesList}
                        basketList={basketList}
                        handlerModal={handlerModal}
                        removeFavorites={removeFavorites}
                        offerCard={offerCard}
                    />
                }
            />
            <Route
                path="basket"
                element={
                    <PageBasket
                        favoritesList={favoritesList}
                        basketList={basketList}
                        handlerModal={handlerModal}
                        addFavorites={addFavorites}
                        removeFavorites={removeFavorites}
                        offerCard={offerCard}
                    />
                }
            />
            <Route path="*" element={<NotPage />} />
        </Routes>
    );
}

AppRoutes.propTypes = {
    goods: PropTypes.array,
    favoritesList: PropTypes.array,
    basketList: PropTypes.array,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    offerCard: PropTypes.func,
};

AppRoutes.defaultProps = {
    goods: [],
    favoritesList: [],
    basketList: [],
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    offerCard: () => {},
};
