import { shallowEqual, useSelector } from "react-redux";

import GoodsList from "../../components/Goods/GoodsList";

export default function PageBasket() {
    const currentBasket = useSelector(
        (state) => state.basket.basketList,
        shallowEqual
    );
    if (currentBasket.length === 0) {
        return (
            <h3 style={{ display: "flex", justifyContent: "center" }}>
                Кошик порожній
            </h3>
        );
    } else {
        return <GoodsList items={currentBasket} btnDeleteBasket />;
    }
}
