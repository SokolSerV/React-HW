import PropTypes from "prop-types";

import GoodsCard from "../GoodsCard";

import "./GoodsList.scss";

export default function GoodsList({ items, btnAddBasket, btnDeleteBasket }) {
    const arrayItems = items.map((card) => {
        return (
            <GoodsCard
                key={card.id}
                card={card}
                btnAddBasket={btnAddBasket}
                btnDeleteBasket={btnDeleteBasket}
            />
        );
    });

    return <div className="goods_wrapper">{arrayItems}</div>;
}

GoodsList.propTypes = {
    items: PropTypes.array,
    btnAddBasket: PropTypes.bool,
    btnDeleteBasket: PropTypes.bool,
};
GoodsList.defaultProps = {
    items: [],
    btnAddBasket: false,
    btnDeleteBasket: false,
};
