import { shallowEqual, useSelector } from "react-redux";

import GoodsList from "../../components/Goods/GoodsList";

export default function PageHome() {
    const goods = useSelector((state) => state.goods.goodsList, shallowEqual);

    return <GoodsList items={goods} btnAddBasket />;
}
